uniform vec2 ures;
uniform float a;

void main() {
    vec2 st = gl_FragCoord.xy / ures - vec2(0.5, 1.0);
    float val = abs(pow(pow(st.x, 2) + pow(st.y, 2) - a*st.x, 2) - pow(a, 2) * (pow(st.x, 2) + pow(st.y, 2))) < 0.001;
    gl_FragColor = vec4(val, 0.0, 0.0, 1.0);
}