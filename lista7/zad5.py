from OpenGL.GL import *
from OpenGL.GL.shaders import compileProgram, compileShader
from app import Application
from model import Model

class Zad5App(Application):
	flat_surf = Model(
		vertices=[
			(-1, -1, 0), (1, -1, 0),
			(1, 1, 0), (-1, 1, 0)
		],
		triangles=[
			(0, 1, 2),
			(0, 2, 3)
		]
	)

	a_var = 0.5
	display_mode = 1

	basic_shader = None
	basic_ures = None

	cardioid_shader = None
	cardioid_ures = None
	cardioid_a = None

	def init_resources(self):
		with open("basic.glsl", "r") as file:
			basic_content = file.read()

		self.basic_shader = compileProgram(compileShader(basic_content, GL_FRAGMENT_SHADER))
		self.basic_ures = glGetUniformLocation(self.basic_shader, "ures")

		with open("cardioid.glsl", "r") as file:
			cardioid_content = file.read()

		self.cardioid_shader = compileProgram(compileShader(cardioid_content, GL_FRAGMENT_SHADER))
		self.cardioid_ures = glGetUniformLocation(self.cardioid_shader, "ures")
		self.cardioid_a = glGetUniformLocation(self.cardioid_shader, "a")

	def on_keyboard(self, key, x, y):
		if key == b"b":
			self.display_mode = 1
		elif key == b"c":
			self.display_mode = 2
		elif key == b"-":
			if self.a_var > 0.01:
				self.a_var -= 0.01
		elif key == b"=":
			if self.a_var < 1.0:
				self.a_var += 0.01

	def on_render(self):
		if self.display_mode == 1:
			glUseProgram(self.basic_shader)
			glUniform2f(self.basic_ures, self.resolution[0]//2, self.resolution[1]//2)

		elif self.display_mode == 2:
			glUseProgram(self.cardioid_shader)
			glUniform2f(self.cardioid_ures, self.resolution[0]//2, self.resolution[1]//2)
			glUniform1f(self.cardioid_a, self.a_var)

		glBegin(GL_QUADS)
		glVertex3f(-1, -1, -1)
		glVertex3f(1, -1, -1)
		glVertex3f(1, 1, -1)
		glVertex3f(-1, 1, -1)
		glEnd()


if __name__ == "__main__":
	Zad5App(
		resolution=(1024, 768),
		clear_color=(0, 0, 0, 1),
		title=b"Lista nr 7, zadanie 5"
	).run()
