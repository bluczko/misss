from OpenGL.GL import glTranslate, glMultMatrixd, GL_TRIANGLES, GL_POLYGON, GL_QUADS
from numpy import sin, cos, random, eye, linalg, matrix, cross, pi
from app import Application
from model import Model
from texture import Texture

def vec_norm(vec):
	return vec / linalg.norm(vec)

def lookat_matrix(cam, target, up=matrix((0, 1, 0))):
	if type(cam) is not matrix:
		cam = matrix(cam)

	if type(target) is not matrix:
		target = matrix(target)

	if type(up) is not matrix:
		up = matrix(up)

	mtx = eye(4)
	fwd = vec_norm(target - cam)
	side = vec_norm(cross(fwd, up))
	up = cross(side, fwd)

	mtx[0:3, 0] = side
	mtx[0:3, 1] = up
	mtx[0:3, 2] = -fwd

	return mtx

def look_at(cx, cy, cz, tx, ty, tz, ux=0, uy=1, uz=0):
	glMultMatrixd(lookat_matrix(
		matrix((cx, cy, cz)),
		matrix((tx, ty, tz)),
		matrix((ux, uy, uz))
	))

	glTranslate(-cx, -cy, -cz)


class Zad4App(Application):
	mouse_pos = 0, 0
	ico = Model.create_icosahedron()
	tex1 = None
	tex2 = None

	def init_resources(self):
		self.tex1 = Texture("tex1.bmp", 1)
		self.tex2 = Texture("tex2.bmp", 2)
		self.ico.uvs = random.rand(len(self.ico.triangles)*3, 2)
		self.ico.texture_enabled[0] = False

	def on_motion(self, x, y):
		self.mouse_pos = x, y

	def on_render(self):
		mx = (self.mouse_pos[0] / self.resolution[0]) * 2 * pi
		my = (self.mouse_pos[1] / self.resolution[1]) * pi
		cam_pos = vec_norm((cos(mx), -cos(my), sin(mx))) * 8

		# gluLookAt(*cam_pos, 0, 0, 0, 0, 1, 0)
		look_at(*cam_pos, 0, 0, 0)

		self.ico.render(mode=GL_TRIANGLES, tex_id=self.tex1.tex_id)


if __name__ == "__main__":
	Zad4App(
		resolution=(1024, 768),
		clear_color=(0, 0, 0, 1),
		title=b"Lista nr 7, zadanie 4"
	).run()
