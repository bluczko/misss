from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *
import numpy as np
from app import Application
from model import Model

class Zad2App(Application):
	pos = [-4, -2, -12]
	mouse_pos = (400, 300)

	models = [
		Model(
			vertices=[
				[0, 1, 0],
				[1.205, -1, 0],
				[-1.205, -1, 0]
			],
			triangles=[[0, 1, 2]],
			colors=[np.random.rand(4, 1) for _ in range(3)]
		),

		Model(
			vertices=[
				[0, 1, 0],
				[1.205, -1, 0],
				[-1.205, -1, 0]
			],
			triangles=[[0, 1, 2]],
			colors=[np.random.rand(4, 1) for _ in range(3)]
		),

		Model(
			vertices=[
				[0, 1, 0],
				[1.205, -1, 0],
				[-1.205, -1, 0]
			],
			triangles=[[0, 1, 2]],
			colors=[np.random.rand(4, 1) for _ in range(3)]
		),

		Model(
			vertices=[
				[-1, 1, 0],
				[1, 1, 0],
				[1, -1, 0],
				[-1, -1, 0]
			],
			triangles=[[0, 1, 2], [2, 3, 0]],
			colors=[np.random.rand(4, 1) for _ in range(4)]
		),

		Model.create_ngon(6, colors=[np.random.rand(4, 1) for _ in range(7)])
	]

	translations = [
		[0, 0, 0],
		[2, 2, 2],
		[2, 2, 2],
		[3, -2, 0],
		[0, -3, -3]
	]

	t = 0

	def on_motion(self, x, y):
		self.mouse_pos = (x, y)

	def on_render(self):
		self.t += 0.01
		self.pos = (-3+np.cos(self.t), -3+np.sin(self.t), -12)

		glTranslate(*self.pos)
		glRotate(-400 + self.mouse_pos[0], 0, 1, 0)
		glRotate(-300 + self.mouse_pos[1], 1, 0, 0)

		self.enable_wireframe(False)

		for i in range(len(self.models)):
			model = self.models[i]
			glTranslate(*self.translations[i])
			model.render()


if __name__ == "__main__":
	Zad2App(
		resolution=(1024, 768),
		clear_color=(0, 0, 0, 1),
		title=b"Lista nr 7, zadanie 2"
	).run()
