from OpenGL.GL import *
from OpenGL.GLU import *
from numpy import cos, sin, random
from app import Application
from model import Model

class Zad3App(Application):
	rotation_mode = 1
	color = (1, 1, 1, 1)

	cube = Model.create_cube(1)
	ngon = Model.create_ngon(6, height=3)

	d = 10
	t = 0
	dt = 0.01

	ct = 0
	nt = 0

	def __init__(self, **kwargs):
		super(Zad3App, self).__init__(**kwargs)

		self.cube.vertices += [-1, 0, 0]
		self.ngon.vertices += [1, 0, 0]

	def on_render(self):
		if self.rotation_mode == 1:
			self.ct += self.dt
			self.nt += self.dt

		elif self.rotation_mode == 2:
			self.nt += self.dt

		elif self.rotation_mode == 3:
			self.ct += self.dt

		crot = self.d * cos(self.ct), 0, self.d * sin(self.ct)
		nrot = self.d * cos(self.nt), 0, self.d * sin(self.nt)

		gluLookAt(*crot, 0, 0, 0, 0, 1, 0)
		self.cube.render()
		glLoadIdentity()

		gluLookAt(*nrot, 0, 0, 0, 0, 1, 0)
		self.ngon.render()
		glLoadIdentity()

	def on_keyboard(self, key, x, y):
		if key in [b"1", b"2", b"3", b"4"]:
			self.rotation_mode = int(key)
		elif key == b"r":
			self.cube.colors = (1, 0, 0, 1)
			self.ngon.colors = (1, 0, 0, 1)
		elif key == b"g":
			self.cube.colors = (0, 1, 0, 1)
			self.ngon.colors = (0, 1, 0, 1)
		elif key == b"b":
			self.cube.colors = (0, 0, 1, 1)
			self.ngon.colors = (0, 0, 1, 1)
		elif key == b"w":
			self.cube.colors = (1, 1, 1, 1)
			self.ngon.colors = (1, 1, 1, 1)
		elif key == b"v":
			self.cube.colors = [random.rand(4, 1) for _ in range(self.cube.vertices.shape[0])]
			self.ngon.colors = [random.rand(4, 1) for _ in range(self.ngon.vertices.shape[0])]
		elif key == b"[":
			self.cube = Model.create_cube(1)
			self.cube.vertices += [-1, 0, 0]

			self.ngon = Model.create_ngon(4, radius=1, height=3)
			self.ngon.vertices += [1, 0, 0]

		elif key == b"]":
			self.cube = Model.create_cube(2)
			self.cube.vertices += [-1, 0, 0]

			self.ngon = Model.create_ngon(4, radius=2, height=6)
			self.ngon.vertices += [1, 0, 0]


if __name__ == "__main__":
	Zad3App(
		resolution=(1024, 768),
		clear_color=(0, 0, 0, 1),
		title=b"Lista nr 7, zadanie 3"
	).run()
