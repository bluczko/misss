uniform vec2 ures;

void main() {
    vec2 st = gl_FragCoord.xy / ures;
    float val = abs(sin(st.x * 10) / 2 + 0.5 - st.y) < 0.1;
    gl_FragColor = vec4(val, 0.0, 0.0, 1.0);
}