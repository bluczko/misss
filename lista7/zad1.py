from OpenGL.GL import *
from app import Application

class Zad1App(Application):
	def on_render(self):
		glBegin(GL_POLYGON)
		glColor(1, 1, 0)
		glVertex3f(1, 0, -3)
		glVertex3f(0, 1, -3)
		glVertex3f(0, 0, -3)
		glEnd()


if __name__ == "__main__":
	Zad1App(
		resolution=(800, 800),
		clear_color=(1, 0, 0, 1),
		title=b"Lista nr 7, zadanie 1"
	).run()
