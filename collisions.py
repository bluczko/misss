import numpy as np
from model import Model
from utils import vec_norm


def aabb_collision(p0, p1, q0, q1, dims=None):
	"""Kolizja Axis-Aligned Bounding Box dla pudel o krancach (p0, p1) i (q0, q1)"""
	if dims is None:
		dims = min([len(point) for point in [p0, p1, q0, q1]])

	return not any([p1[i] < q0[i] or p0[i] > q1[i] for i in range(dims)])


def aabb_corners_for(model):
	"""Tworzy punkty krancowe AABB dla wybranego modelu"""
	v0 = np.min(model.vertices, axis=0)
	v1 = np.max(model.vertices, axis=0)
	return v0, v1


def aabb_render(model, color=(1, 1, 1, 1)):
	"""Rysuje AABB dla danego modelu"""
	model_minmax = aabb_corners_for(model)
	Model.create_box(*model_minmax, colors=color).render(wireframe=True)


def aabb_model_collision_2d(m0, m1, render_box=False):
	"""Tworzy AABB dla modeli 2D i sprawdza, czy sie przecinaja"""
	m0_minmax = aabb_corners_for(m0)
	m1_minmax = aabb_corners_for(m1)

	collision_detected = aabb_collision(*m0_minmax, *m1_minmax, dims=2)

	if render_box:
		# UWAGA: Malo wydajne przy duzej ilosci boxow
		c = (1, 0, 0, 1) if collision_detected else (0, 1, 0, 1)
		Model.create_quad(*m0_minmax, colors=c).render(wireframe=True)
		Model.create_quad(*m1_minmax, colors=c).render(wireframe=True)

	return collision_detected


def aabb_model_collision_3d(m0, m1, render_box=False):
	"""Tworzy AABB dla modeli 3D i sprawdza, czy sie przecinaja"""
	m0_minmax = aabb_corners_for(m0)
	m1_minmax = aabb_corners_for(m1)

	collision_detected = aabb_collision(*m0_minmax, *m1_minmax, dims=3)

	if render_box:
		# UWAGA: Malo wydajne przy duzej ilosci boxow
		c = (1, 0, 0, 1) if collision_detected else (0, 1, 0, 1)
		Model.create_box(*m0_minmax, colors=c).render(wireframe=True)
		Model.create_box(*m1_minmax, colors=c).render(wireframe=True)

	return collision_detected


def line_from_points(a, b):
	"""Zwroc wyrazenie lambda wykonujace funkcje liniowa o parametrach wywnioskowanych z (a,b)"""
	a = (b[1] - a[1]) / (b[0] - a[0])
	b = a[1] - a * a[1]
	return lambda x: a * x + b


def point_on_line(a, b, p):
	"""Czy punkt p lezy na prostej (l0, l1)"""
	if p[0] < a[0] or p[0] > b[0] or p[1] < a[1] or p[1] > b[1]:
		return False

	f = line_from_points(a, b)
	return f(p[0]) == p[1]


def lines_cross(a, b, c, d):
	"""Czy linia z punktow (a, b) przecina sie z linia z punktow (c, d)"""

	# Najpierw policz mianowniki
	t_den = (b[0] - a[0]) * (c[1] - d[1]) - (b[1] - a[1]) * (c[0] - d[0])
	s_den = (b[0] - a[0]) * (c[1] - d[1]) - (b[1] - a[1]) * (c[0] - d[0])

	# Nie ma sensu liczyc dalej, jesli mianownik == 0
	if t_den == 0 or s_den == 0:
		return False

	# Oblicz liczniki
	t_nom = (c[0] - a[0]) * (c[1] - d[1]) - (c[1] - a[1]) * (c[0] - d[0])
	s_nom = (b[0] - a[0]) * (c[1] - a[1]) - (c[0] - a[0]) * (b[1] - a[1])

	t = t_nom / t_den
	s = s_nom / s_den

	# Linie sie przecinaja jezeli spelnione sa ponizsze warunki
	return 0 <= t <= 1 and 0 <= s <= 1


def point_in_triangle_2d(tri, p, strict_check=False):
	"""Sprawdza, czy punkt p jest zawarty w trojkacie tri"""
	a, b, c = tri

	# Na razie policz tylko mianowniki
	alpha_den = (b[0] - a[0]) * (c[1] - a[1]) - (c[0] - a[0]) * (b[1] - a[1])
	beta_den = (b[0] - a[0]) * (c[1] - a[1]) - (c[0] - a[0]) * (b[1] - a[1])

	# Jesli ktorykolwiek z mianownikow == 0, to trojkat jest zdeformowany i nie ma sensu liczyc dalej
	if alpha_den == 0 or beta_den == 0:
		if not strict_check:
			return False

		# Mozna opcjonalnie sprawdzic, czy punkt p jest na linii ab, bc lub ac
		return point_on_line(a, b, p) or point_on_line(b, c, p) or point_on_line(a, c, p)

	# Skoro mianowniki != 0, to mozna przejsc do licznikow
	alpha_nom = (p[0] - a[0]) * (c[1] - a[1]) - (c[0] - a[0]) * (p[1] - a[1])
	beta_nom = (b[0] - a[0]) * (p[1] - a[1]) - (p[0] - a[0]) * (b[1] - a[1])

	alpha = alpha_nom / alpha_den
	beta = beta_nom / beta_den

	# Kolizja istnieje, jesli wsp. alpha i beta spelniaja okreslone warunki
	return alpha > 0 and beta > 0 and (alpha + beta) <= 1


def triangle_collision_2d(tri0, tri1, strict_check=False):
	"""Sprawdza, czy trojkat tri1 jest w trojkacie tri0"""
	if any([point_in_triangle_2d(tri0, p, strict_check) for p in tri1]):
		return True

	tri_checks = [(0, 1), (1, 2), (0, 2)]

	for i in tri_checks:
		for j in tri_checks:
			a, b = tri0[i[0]], tri0[i[1]]
			c, d = tri1[j[0]], tri1[j[1]]
			if lines_cross(a, b, c, d):
				return True

	return False


def model_inside_other_2d(m0, m1):
	"""Sprawdzenie, czy model m0 znajduje sie wewnatrz modelu m1"""
	points_inside = [False] * m0.vertices.shape[0]  # tyle elementow ile punktow

	for i in range(len(m0.triangles)):
		tri0 = m0.triangle(i)

		for j in range(len(m1.triangles)):
			tri1 = m1.triangles[j]

			for p in tri1:
				if not points_inside[p]:
					if point_in_triangle_2d(tri0, m1.vertices[p, :]):
						points_inside[p] = True

	return all(points_inside)


def model_collision_2d(m0, m1, strict_check=False):
	"""Sprawdza, czy modele m0 i m1 koliduja ze soba, biorac pod uwage osie X i Y"""
	for i in range(len(m0.triangles)):
		tri0 = m0.triangle(i)

		for j in range(len(m1.triangles)):
			tri1 = m1.triangle(j)

			if triangle_collision_2d(tri0, tri1, strict_check):
				return True

	return False


def plane_side(a, b, c, p):
	"""Gdzie punkt p znajduje sie wzgledem plaszczyzny trojkata abc"""
	return np.linalg.det(np.matrix([a, b, c]) - np.matrix(p))


def plane_line_intersection_3d(tri, l0, l1):
	"""
	Czy trojkat tri przecina sie z odcinkiem o krancach [l0, l1]
	:returns krotka z informacjami czy istnieje przeciecie i punkt przeciecia plaszczyzny z prosta odcinka
	"""
	a, b, c = tri
	v = vec_norm(np.cross(b - a, c - a))
	u = v @ b

	t_den = v @ (l1 - l0)

	if t_den == 0:
		# Prosta rownolegla do plaszczyzny
		return False, None

	t_nom = u - v @ l0
	t = t_nom / t_den

	# Punkt przeciecia plaszczyzny i prostej
	x = l0 + t * (l1 - l0)

	return 0 <= t <= 1, x


def triangle_collision_3d(tri0, tri1, callback=None):
	"""Czy przecinaja sie trojkaty tri0 i tri1"""
	sides = [plane_side(*tri0, i) for i in tri1]

	if all(s == 0 for s in sides):
		return True

	if all(s > 0 for s in sides) or all(s < 0 for s in sides):
		return False

	for i in [(0, 1), (1, 2), (0, 2)]:
		collision, intersection_point = plane_line_intersection_3d(tri0, tri1[i[0]], tri1[i[1]])
		if collision:
			if callback is not None:
				callback(collision, intersection_point)
			return point_in_triangle_2d(tri0, intersection_point)

	return False


def model_collision_3d(m0, m1, callback=None):
	"""Czy koliduja ze soba modele m0 i m1"""
	for i in range(len(m0.triangles)):
		tri0 = m0.triangle(i)

		for j in range(len(m1.triangles)):
			tri1 = m1.triangle(j)

			if triangle_collision_3d(tri0, tri1, callback):
				return True

	return False
