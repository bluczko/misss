import numpy as np
from OpenGL.GL import *
from utils import rotation_matrix

class Model(object):
	def __init__(self, vertices=(), triangles=(), uvs=None, colors=(1, 1, 1, 1)):
		self.vertices = np.array(vertices, dtype=np.float32)
		self.triangles = triangles
		self.colors = colors
		self.uvs = uvs

	def translate(self, pos):
		self.vertices += pos

	def rotate(self, rad, axis=(0, 0, 1), center=None):
		rot_mtx = rotation_matrix(rad, axis)

		if center is None:
			center = self.center

		for i in range(self.vertices.shape[0]):
			self.vertices[i, :] = ((self.vertices[i, :] - center) @ rot_mtx) + center

	@staticmethod
	def __str_to_mode(mode_str):
		if mode_str in ["tri", "tris", "triangles"]:
			return GL_TRIANGLES
		elif mode_str == "quads":
			return GL_QUADS
		elif mode_str in ["poly", "polygon"]:
			return GL_POLYGON

	def render(self, mode=GL_POLYGON, tex_id=-1, wireframe=False):
		if type(mode) == str:
			mode = Model.__str_to_mode(mode)

		if tex_id >= 0:
			glBindTexture(GL_TEXTURE_2D, tex_id)

		if wireframe:
			glPolygonMode(GL_FRONT_AND_BACK, GL_LINE)

		glBegin(mode)
		for i in range(len(self.triangles)):
			triangle = self.triangles[i]

			for vertex in triangle:
				if tex_id >= 0 and self.uvs is not None:
					glTexCoord2f(*self.uvs[vertex])

				if type(self.colors) == tuple:
					glColor(*self.colors)  # jesli mamy jeden kolor
				elif type(self.colors) == list:
					glColor(*self.colors[vertex])  # jesli mamy liste kolorow

				glVertex3d(*self.vertices[vertex])

		glEnd()

		if wireframe:
			glPolygonMode(GL_FRONT_AND_BACK, GL_FILL)

	def triangle(self, i):
		"""Wierzcholki i-tego trojkata (jako krotka)"""
		tri = self.triangles[i]
		return self.vertices[tri[0]], self.vertices[tri[1]], self.vertices[tri[2]]

	@property
	def tris_len(self):
		"""Ilosc trojkatow"""
		return len(self.triangles)

	@property
	def vert_len(self):
		"""Ilosc wierzcholkow"""
		return self.vertices.shape[0]

	@property
	def center(self):
		"""Srodek figury wyznaczony z pozycji wierzcholkow"""
		return self.vertices.mean(axis=0)

	@staticmethod
	def create_ngon(n, radius=1, height=0, **kwargs):
		vertices = [(0, height, 0)]
		triangles = []

		for i in range(n):
			r = 2 * np.pi * (i/n)
			vertices.append((np.cos(r) * radius, 0, np.sin(r) * radius))
			triangles.append((0, i+1, i+2) if i < n-1 else (0, n, 1))

		return Model(vertices, triangles, **kwargs)

	@staticmethod
	def create_icosahedron(**kwargs):
		x, z, n = 0.525731112119133606, 0.850650808352039932, 0

		vertices = [
			(-x, n, z), (x, n, z),
			(-x, n, -z), (x, n, -z),
			(n, z, x), (n, z, -x),
			(n, -z, x), (n, -z, -x),
			(z, x, n), (-z, x, n),
			(z, -x, n), (-z, -x, n)
		]

		triangles = [
			(0, 4, 1), (0, 9, 4), (9, 5, 4), (4, 5, 8),
			(4, 8, 1), (8, 10, 1), (8, 3, 10), (5, 3, 8),
			(5, 2, 3), (2, 7, 3), (7, 10, 3), (7, 6, 10),
			(7, 11, 6), (11, 0, 6), (0, 1, 6), (6, 1, 10),
			(9, 0, 11), (9, 11, 2), (9, 2, 5), (7, 2, 11)
		]

		return Model(vertices, triangles, **kwargs)

	@staticmethod
	def create_tetrahedron(v0, v1, v2, v3, **kwargs):
		return Model(
			vertices=[v0, v1, v2, v3],
			triangles=[
				(0, 1, 2), (0, 2, 3),
				(0, 1, 3), (1, 2, 3)
			],
			**kwargs
		)

	@staticmethod
	def create_quad(c0, c1, z=0, **kwargs):
		return Model(
			vertices=[
				(c0[0], c0[1], z),
				(c1[0], c0[1], z),
				(c1[0], c1[1], z),
				(c0[0], c1[1], z),
			],
			triangles=[(0, 1, 2), (2, 3, 0)],
			**kwargs
		)

	@staticmethod
	def create_box(c0, c1, **kwargs):
		vertices = np.array([
			(c0[0], c0[1], c0[2]),
			(c1[0], c0[1], c0[2]),
			(c1[0], c1[1], c0[2]),
			(c0[0], c1[1], c0[2]),
			(c0[0], c0[1], c1[2]),
			(c1[0], c0[1], c1[2]),
			(c1[0], c1[1], c1[2]),
			(c0[0], c1[1], c1[2]),
		])

		triangles = [
			(2, 1, 0), (0, 3, 2),
			(4, 5, 6), (6, 7, 4),
			(1, 5, 6), (6, 2, 1),
			(0, 4, 7), (7, 3, 0),
			(3, 2, 6), (6, 7, 3),
			(0, 1, 5), (5, 4, 0)
		]

		return Model(vertices, triangles,  **kwargs)

	@staticmethod
	def create_cube(size, **kwargs):
		s = size * 0.5
		return Model.create_box((-s, -s, -s), (s, s, s), **kwargs)
