import numpy as np


class MouseControllerMixin:
	cam_dist = 30
	t = 0

	def on_mouse(self, button, state, x, y):
		if state:
			if button == 3:
				self.cam_dist = max(10.0, self.cam_dist * 0.95)

			if button == 4:
				self.cam_dist = min(100.0, self.cam_dist * 1.05)

	def on_motion(self, x, y):
		self.t = (x / self.resolution[0] - 0.5) * 4 * np.pi


def vec_norm(vec):
	"""Wektor znormalizowany"""
	return vec / np.linalg.norm(vec)

def vec_interp(v0, v1, a):
	"""Interpolacja wektora między wektorami v0 i v1"""
	return v0 + a * (v1 - v0)


def xyz2ntk(v0, v1, norm=False):
	"""Przejscie z ukladu XYZ na ntk. Zwraca macierz M"""
	n = v1 - v0

	nabs_min = np.argsort(np.abs(n))[0]
	if nabs_min == 0:
		t = vec_norm(np.array([0, n[2], -n[1]]))
	elif nabs_min == 1:
		t = vec_norm(np.array([-n[2], 0, n[0]]))
	else:  # nabs_min == 2
		t = vec_norm(np.array([n[1], -n[0], 0]))
	k = np.cross(n, t)

	m = np.array([n, t, -k])

	return vec_norm(m) if norm else m


def rotation_matrix(rad, axis):
	"""Tworzy macierz obrotu o kąt rad po osi axis"""
	sr, cr = np.sin(rad), np.cos(rad)
	icr = 1 - cr
	u = axis / np.linalg.norm(axis)

	return np.matrix([
		[
			cr + u[0] ** 2 * icr,
			u[0] * u[1] * icr - u[2] * sr,
			u[0] * u[2] * icr + u[1] * sr
		],
		[
			u[1] * u[0] * icr + u[2] * sr,
			cr + u[1] ** 2 * icr,
			u[1] * u[2] * icr - u[0] * sr
		],
		[
			u[2] * u[0] * icr - u[1] * sr,
			u[2] * u[1] * icr + u[0] * sr,
			cr + u[2] ** 2 * icr
		]
	], dtype=np.float32)


def point_plane_dist(p, tri):
	"""Odleglosc punktu od plaszczyzny"""
	a, b, c = vec_norm(np.cross(tri[0] - tri[1], tri[1] - tri[2]))
	return np.abs(a * (p[0] - tri[0][0]) + b * (p[1] - tri[0][1]) + c * (p[2] - tri[0][2]))


def r3_matrix(a, b, g):
	"""Macierz obrotu alfa/beta/gamma"""
	return np.array([
		[
			np.cos(g) * np.cos(a) - np.cos(b) * np.sin(a) * np.sin(g),
			np.cos(g) * np.sin(a) + np.cos(b) * np.cos(a) * np.sin(g),
			np.sin(g) * np.sin(b)
		],
		[
			-np.sin(g) * np.cos(a) - np.cos(b) * np.sin(a) * np.cos(g),
			-np.sin(g) * np.sin(a) + np.cos(b) * np.cos(a) * np.cos(g),
			np.cos(g) * np.sin(b)
		],
		[
			np.sin(b) * np.sin(a),
			-np.sin(b) * np.cos(a),
			np.cos(b)
		]
	], dtype=np.float32)

def gs_ortho(v):
	"""Ortogonalizacja Grama-Schmidta"""
	u0 = vec_norm(v[0])
	u1 = vec_norm(v[1] - np.dot(v[1], u0) * u0)
	u2 = vec_norm(v[2] - np.dot(v[2], u0) * u0 + np.dot(v[2], u1) * u1)
	return np.array([u0, u1, u2])

def clamp(vmin, value, vmax):
	"""Ogranicz wartość do przedziału [vmin; vmax]"""
	return np.max([vmin, np.min([value, vmax])])
