from PIL import Image
from OpenGL.GL import *

class Texture(object):
	def __init__(self, file, tex_id):
		self.tex_id = tex_id
		image = Image.open(file)
		w, h = image.size
		image = image.tobytes("raw", "RGBX", 0, -1)

		glBindTexture(GL_TEXTURE_2D, self.tex_id)
		glTexImage2D(GL_TEXTURE_2D, 0, 3, w, h, 0, GL_RGBA, GL_UNSIGNED_BYTE, image)
		glTexParameter(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST)
