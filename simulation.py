import numpy as np
from OpenGL.GL import *
from OpenGL.GLU import *
from utils import vec_norm, clamp


class PlaneCollider:
	def __init__(self, axis=0, dist=0, size=(10, 10)):
		self.axis = axis

		if type(size) is int or type(size) is float:
			size = (size, size)

		# default: YZ (normal: axis X)
		v = np.array([
			[dist, -size[0], -size[1]],
			[dist, size[0], -size[1]],
			[dist, size[0], size[1]],
			[dist, -size[0], size[1]]
		])

		# XZ (normal: axis Y) swap X->Y
		if self.axis == 1:
			v[:, (0, 1)] = v[:, (1, 0)]

		# XY (normal: axis Z) swap X->Z
		if self.axis == 2:
			v[:, (0, 2)] = v[:, (2, 0)]

		self.vertices = v

	@property
	def dist(self):
		return self.vertices[0, self.axis]

	@property
	def negative_dist(self):
		return self.dist >= 0

	def render(self, color=(1, 1, 1)):
		glColor3f(*color)
		glBegin(GL_QUADS)
		for i in range(self.vertices.shape[0]):
			glVertex3fv(self.vertices[i, :])
		glEnd()


class Sphere:
	max_velocity = 50

	def __init__(
		self,
		position=(0, 0, 0), velocity=(0, 0, 0), color=(1, 1, 1),
		radius=1.0, mass=1.0, spring=1.0, aero=1.0, grav=0.0
	):
		self.position = np.array(position, dtype=np.float32)
		self.velocity = np.array(velocity, dtype=np.float32)
		self.color = tuple(color) if type(color) is not tuple else color
		self.radius = radius
		self.mass = mass
		self.springiness = spring
		self.aerodynamic = aero
		self.gravity = grav

		self.quadratic = gluNewQuadric()
		gluQuadricNormals(self.quadratic, GLU_SMOOTH)

	def update_position(self, delta_time):
		a = (self.velocity**2 / self.mass) * self.aerodynamic
		self.velocity -= np.sign(self.velocity) * a * delta_time
		self.velocity -= np.array([0, self.gravity * self.mass, 0])

		self.position += delta_time * self.velocity

	def mod_springiness(self, amount):
		"""Zmiana wsp. sprezystosci w rozsadnych granicach"""
		self.springiness = clamp(0, self.springiness + amount, 2)

	def mod_gravity(self, amount):
		"""Zmiana przyciagania grawitacyjnego w rozsadnych granicach"""
		self.gravity = clamp(0, self.gravity + amount, 10)

	def mod_aerodynamic(self, amount):
		"""Zmiana wsp. aerodynamiki w rozsadnych granicach"""
		self.aerodynamic = clamp(0, self.aerodynamic + amount, 1)

	def sim_plane_coll(self, plane):
		"""Kolizja z plaszczyzna"""
		if np.abs(self.position[plane.axis] - plane.dist) <= self.radius:
			self.velocity[plane.axis] *= -1 * self.springiness
			self.position[plane.axis] = plane.dist - self.radius * np.sign(plane.dist)

	def sim_sphere_coll(self, other, dt):
		"""Kolizja z inna sfera"""
		pos_diff = other.position - self.position
		min_dist = other.radius + self.radius

		if np.linalg.norm(pos_diff) <= min_dist:
			self.position -= self.velocity * dt
			other.position -= other.velocity * dt

			vel_diff = other.velocity - self.velocity
			# Wzor A 1.1
			n = vec_norm(pos_diff)
			if vel_diff @ n < 0:
				n *= -1  # odwroc zwrot wektora

			# Wzor A 1.2
			nabs_min = np.argsort(np.abs(n))[0]
			if nabs_min == 0:
				t = vec_norm(np.array([0, n[2], -n[1]]))
			elif nabs_min == 1:
				t = vec_norm(np.array([-n[2], 0, n[0]]))
			else:  # nabs_min == 2
				t = vec_norm(np.array([n[1], -n[0], 0]))
			k = np.cross(n, t)

			m = vec_norm(np.array([n, t, k]))

			u0 = m @ self.velocity
			u1 = m @ other.velocity

			i = self.mass * other.mass * (u1[0] - u0[0]) * (1 + self.springiness)
			i /= self.mass + other.mass

			v0 = np.array([(i + self.mass * u0[0]) / self.mass, u0[1], u0[2]])
			v1 = np.array([(-i + other.mass * u1[0]) / other.mass, u1[1], u1[2]])

			self.velocity = v0 @ np.linalg.inv(m)
			other.velocity = v1 @ np.linalg.inv(m)

	def sim_collision(self, other_collider, dt):
		"""Automatycznie wykryj rodzaj kolizji i zastosuj ja"""
		ctype = type(other_collider)

		if ctype == PlaneCollider:
			self.sim_plane_coll(other_collider)
		elif ctype == Sphere:
			self.sim_sphere_coll(other_collider, dt)

	def render(self, res=24):
		glPushMatrix()
		glTranslatef(*self.position)
		glColor3f(*self.color)
		gluSphere(self.quadratic, self.radius, res, res)
		glPopMatrix()

class Stick:
	min_dist, max_dist = 0, 0
	max_force = 120.0
	followed_sphere = None
	quadratic = None
	angle = 0.0
	dist = 0.0

	def __init__(self, sphere):
		self.set_by(sphere)

		self.min_dist = float(sphere.radius + 0.2)
		self.max_dist = float(self.min_dist + 4)

		self.dist = self.min_dist

		self.quadratic = gluNewQuadric()
		gluQuadricNormals(self.quadratic, GLU_SMOOTH)

	def set_by(self, sphere):
		self.followed_sphere = sphere

	def add_angle(self, a):
		self.angle += a

	def add_dist(self, d):
		self.dist = clamp(self.min_dist, self.dist + d, self.max_dist)

	def hit_sphere(self):
		r = np.deg2rad(self.angle)
		force = (self.dist - self.min_dist) / self.max_dist * self.max_force
		ang_vec = np.array([np.sin(r), 0.1, np.cos(r)]) * force
		self.followed_sphere.velocity -= ang_vec

		# resetuj do wartosci domyslnej
		self.dist = self.min_dist

	def render(self):
		glPushMatrix()
		glTranslate(*self.followed_sphere.position)
		glRotate(self.angle, 0, 1, 0)
		glTranslate(0, 0, self.dist)
		glColor(0.7, 0.2, 0)
		gluCylinder(self.quadratic, 0.3, 0.3, 10, 8, 1)
		glColor(0, 0.4, 0)
		gluSphere(self.quadratic, 0.45, 8, 8)
		glPopMatrix()
