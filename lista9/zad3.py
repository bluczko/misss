import numpy as np

from app import Application
from simulation import PlaneCollider, Sphere, Stick
from utils import MouseControllerMixin


class Zad3App(MouseControllerMixin, Application):
	environment = []
	spheres = []

	stick = None

	def init_resources(self):
		self.set_global_light_pos(position=(0, 5, 5))

		self.environment.append(PlaneCollider(axis=1, dist=-10, size=(16, 9)))
		self.environment.append(PlaneCollider(axis=1, dist=10, size=(16, 9)))

		self.environment.append(PlaneCollider(axis=0, dist=-16, size=(10, 9)))
		self.environment.append(PlaneCollider(axis=0, dist=16, size=(10, 9)))

		self.environment.append(PlaneCollider(axis=2, dist=-9, size=(10, 16)))
		self.environment.append(PlaneCollider(axis=2, dist=9, size=(10, 16)))

		np.random.seed(103)

		for _ in range(6):
			self.spheres.append(Sphere(
				position=(np.random.rand(3) * 10) - 5, velocity=np.random.randn(3),
				color=np.random.rand(3),
				mass=6, spring=0.9, aero=0.8, grav=1, radius=1
			))

		self.stick = Stick(sphere=self.spheres[0])

	def on_keyboard(self, key, x, y):
		if key == b"r":
			for sphere in self.spheres:
				sphere.velocity += (np.random.rand(3) - 0.5) * 100

		if key == b"a":
			self.stick.add_angle(-5)

		if key == b"d":
			self.stick.add_angle(5)

		if key == b"s":
			self.stick.add_dist(-0.3)

		if key == b"w":
			self.stick.add_dist(0.3)

		if key == b" ":
			self.stick.hit_sphere()

	def on_render(self):
		dt = self.tick_delta

		self.look_at(
			cam_pos=(self.cam_dist * np.sin(self.t), 5, self.cam_dist * np.cos(self.t)),
			target_pos=(0, 0, 0)
		)

		self.stick.render()

		for i in range(len(self.environment)):
			self.enable_wireframe(i > 0)
			plane = self.environment[i]
			plane.render(color=(1, 1, 1) if i > 0 else (0, 0.7, 0))

			for sphere in self.spheres:
				sphere.sim_plane_coll(plane)
				sphere.render()

		for sphere in self.spheres:
			for other in self.spheres:
				if other != sphere:
					sphere.sim_sphere_coll(other, dt)

		for sphere in self.spheres:
			sphere.update_position(dt)

		self.enable_wireframe(False)


if __name__ == "__main__":
	Zad3App(
		resolution=(1024, 768),
		clear_color=(0, 0, 0, 1),
		title="Lista 9, zadanie 3"
	).run()
