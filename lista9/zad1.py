import numpy as np

from app import Application
from simulation import Sphere, PlaneCollider
from utils import MouseControllerMixin


class Zad12App(MouseControllerMixin, Application):
	"""Scalone zadanie 1 i 2"""

	environment = []
	sphere = None

	def init_resources(self):
		self.set_global_light_pos(position=(0, 5, 5))
		self.sphere = Sphere(
			position=(-5, 2, 3), velocity=(5, -5, 3), color=(1, 0, 0),
			mass=10, spring=0.8, aero=1, grav=0
		)

		wsize, dst = 10, 10

		for axis in [1, 0, 2]:
			for dist in [-dst, dst]:
				self.environment.append(PlaneCollider(axis, dist, wsize))

	def on_keyboard(self, key, x, y):
		if key == b" ":
			add_vel = (np.random.rand(3) - 0.5) * 100
			self.sphere.velocity += add_vel

		if key == b"-":
			print(f"spring: {self.sphere.springiness}")
			self.sphere.mod_springiness(-0.1)

		if key == b"=":  # tak na prawde to +
			print(f"spring: {self.sphere.springiness}")
			self.sphere.mod_springiness(0.1)

		if key == b",":  # <
			print(f"gravity: {self.sphere.gravity}")
			self.sphere.mod_gravity(-0.1)

		if key == b".":  # >
			print(f"gravity: {self.sphere.gravity}")
			self.sphere.mod_gravity(0.1)

		if key == b"[":
			print(f"aero: {self.sphere.aerodynamic}")
			self.sphere.mod_aerodynamic(-0.1)

		if key == b"]":
			print(f"aero: {self.sphere.aerodynamic}")
			self.sphere.mod_aerodynamic(0.1)

	def on_render(self):
		self.look_at(
			cam_pos=(self.cam_dist * np.sin(self.t), 5, self.cam_dist * np.cos(self.t)),
			target_pos=(0, 0, 0)
		)

		self.sphere.render()

		self.enable_wireframe(False)

		for i in range(len(self.environment)):
			self.enable_wireframe(i > 0)
			plane = self.environment[i]
			plane.render(color=(1, 1, 1) if i > 0 else (0, 0.7, 0))
			self.sphere.sim_collision(plane, self.tick_delta)

		self.enable_wireframe(False)

		self.sphere.update_position(self.tick_delta)

		# failsafe, gdyby cos sie zepsulo
		if np.linalg.norm(self.sphere.position) > 20:
			self.sphere.position = np.random.randn(3)
			self.sphere.velocity = np.array([0, 0, 0], dtype=np.float32)


if __name__ == "__main__":
	Zad12App(
		resolution=(1024, 768),
		clear_color=(0, 0, 0, 1),
		title="Lista 9, zadanie 1+2"
	).run()
