import numpy as np
from OpenGL.GL import *

from model import Model
import collisions as coll
import utils

n, t, k = 0, 1, 2  # Wartosci pomocnicze

def h_val(a, b, c, d, r, q):
	return r[a] * q[b, c] * r[d]

class RigidBody:
	def __init__(
		self,
		model: Model,
		density=1.0,
		lin_resist=1.0,
		ang_resist=1.0,
		springiness=1.0,
		friction=0.0
	):

		self.model = model
		self.density = density

		self.lin_resistance = lin_resist
		self.ang_resistance = ang_resist

		self.springiness = springiness
		self.friction = friction

		# predkosc liniowa
		self.lin_velocity = np.zeros(shape=3, dtype=np.float32)
		self._prev_lin_vel = self.lin_velocity

		# predkosc katowa
		self.ang_velocity = np.zeros(shape=3, dtype=np.float32)
		self._prev_ang_vel = self.ang_velocity

		self.volume = self._recalc_volume()
		self.inertia = self._recalc_inertia()

		self.o = np.zeros(shape=3, dtype=np.float32)
		self._prev_o = self.o

	@property
	def mass(self):
		"""Masa ciala (symbol: m)"""
		return self.volume * self.density

	@property
	def mass_center(self):
		"""Pozycja srodka masy ciala (symbol: r(t))"""
		return self.model.center

	@property
	def momentum(self):
		"""Predkosc liniowa srodka masy ciala (symbol: u(t))"""
		return self.lin_velocity / self.mass

	@property
	def delta_lin_vel(self):
		"""Ped srodka masy ciala (symbol: L(t))"""
		return self._prev_lin_vel - self.lin_velocity

	@property
	def delta_o(self):
		"""Wypadkowy moment sily dzialajacej na cialo (symbol: H(t))"""
		return self._prev_o - self.o

	@property
	def j_matrix(self):
		"""Tensor inercji w układzie po rotacji (symbol: J(t))"""
		r = utils.r3_matrix(*self.ang_velocity)
		return r @ self.inertia @ r.transpose()

	@property
	def omega_matrix(self):
		"""Predkosc katowa (symbol: omega)"""
		return np.linalg.inv(self.j_matrix) * self.delta_o

	@property
	def w_matrix(self):
		"""Zmiana macierzy obrotu ze wzoru 17, bez ortogonalizacji G-S (symbol: W(t))"""
		av = self.ang_velocity
		return np.matrix([
			[0, -av[2], av[1]],
			[av[2], 0, -av[0]],
			[-av[1], av[0], 0]
		], dtype=np.float32)

	@property
	def w_matrix_ortho(self):
		return utils.gs_ortho(self.w_matrix)

	def _recalc_volume(self):
		"""
		Przelicza objetosc dla podanego modelu
		https://blenderartists.org/t/volume-mass-calculator/564117
		"""
		part_vol = 0.0
		for i in range(len(self.model.triangles)):
			a, b, c = self.model.triangle(i)
			part_vol += np.abs(np.dot(a, np.cross(b, c)) / 6.0)
		return part_vol

	def _recalc_inertia(self):
		"""
		Przelicza tensor inercji dla sfery (najblizsza aproksymacja dowolnego ciala)
		Promien sfery to srednia odleglosc wierzcholka od srodka modelu
		https://en.wikipedia.org/wiki/List_of_moments_of_inertia
		"""
		center = self.model.vertices.mean(axis=0)
		r = np.linalg.norm(self.model.vertices - center)
		return np.eye(3) * 0.4 * self.mass * r**2

	def __crit_p(self, u0, u1, w0, w1, ms, q0, q1, r0, r1):
		"""Impuls krytyczny przy zrownaniu predkosci (C.4.2)"""
		z_t = u0[t] + r0[n] * w0[k] - r0[k] * w0[n]  # Wzor 75
		z_k = u0[k] - r0[n] * w0[t] + r0[t] * w0[n]  # Wzor 76

		# Skroty do h_val (z podanymi r, q)
		def h0(a, b, c, d): return h_val(a, b, c, d, r0, q0)

		def h1(a, b, c, d): return h_val(a, b, c, d, r1, q1)

		zt_ = np.array([  # Wzory 77, 78, 79
			- h1(n, 2, 1, k) + h1(n, 2, 2, t) + h1(k, 0, 1, k) - h1(k, 0, 2, t)
			- h0(n, 2, 1, k) + h0(n, 2, 2, t) + h0(k, 0, 1, k) - h0(k, 0, 2, t),  # n

			h1(n, 2, 0, k) - h1(n, 2, 2, n) - h1(k, 0, 0, k) + h1(k, 0, 2, n) +
			h0(n, 2, 0, k) - h0(n, 2, 2, n) - h0(k, 0, 0, k) + h0(k, 0, 2, n) - ms,  # t

			h1(n, 2, 0, t) + h1(n, 2, 1, n) + h1(k, 0, 0, t) - h1(k, 0, 1, n) +
			h0(n, 2, 0, t) + h0(n, 2, 1, n) + h0(k, 0, 0, t) - h0(k, 0, 1, n)  # k
		])

		zk_ = np.array([  # Wzory 80, 81, 82
			h1(n, 1, 1, k) - h1(n, 1, 2, t) - h1(t, 0, 1, k) + h1(t, 0, 2, t) +
			h0(n, 1, 1, k) - h0(n, 1, 2, t) - h0(t, 0, 1, k) + h0(t, 0, 2, t),  # n

			- h1(n, 1, 0, k) - h1(n, 1, 2, n) + h1(t, 0, 0, k) - h1(t, 0, 2, n)
			- h0(n, 1, 0, k) - h0(n, 1, 2, n) + h0(t, 0, 0, k) - h0(t, 0, 2, n),  # t

			h1(n, 1, 0, k) - h1(0, 1, 1, 0) - h1(1, 0, 0, 1) + h1(1, 0, 1, 0) +
			h0(n, 1, 0, k) - h0(0, 1, 1, 0) - h0(1, 0, 0, 1) + h0(1, 0, 1, 0) - ms  # k
		])

		a_t = (z_t * zk_[k] - z_k * zt_[k]) / (zt_[t] * zk_[k] - zk_[t] * zt_[k])  # Wzor 83
		b_t = -(zt_[n] * zk_[k] - zk_[n] * zt_[k]) / (zt_[t] * zk_[k] - zk_[t] * zt_[k])  # Wzor 84

		a_k = (z_k * zt_[t] - z_t * zk_[t]) / (zk_[k] * zt_[t] - zt_[k] * zk_[t])  # Wzor 85
		b_k = -(zk_[n] * zt_[t] - zt_[n] * zk_[t]) / (zk_[k] * zt_[t] - zt_[k] * zk_[t])  # Wzor 86

		a0 = np.array([  # Wzor 87
			-r0[k] * a_t + r0[t] * a_k,
			-r0[n] * a_k,
			r0[n] * a_t
		])

		b0 = np.array([  # Wzor 88
			-r0[k] * b_t + r0[t] * b_k,
			r0[k] - r0[n] * b_k,
			-r0[t] + r0[n] * b_t
		])

		a1 = np.array([  # Wzor 89
			-r1[k] * a_t + r1[t] * a_k,
			-r1[n] * a_k,
			r1[n] * a_t
		])

		b1 = np.array([  # Wzor 90
			-r1[k] * b_t + r1[t] * b_k,
			r1[k] - r1[n] * b_k,
			-r1[t] + r1[n] * b_t
		])

		# Wzory 91, 92, 93
		p_n_nom = u1[n] - u0[n] - (r0[k] * w0[t]) + (r0[t] * w0[k]) + (r1[k] * w1[t]) - (r1[t] * w1[k])
		p_n_nom -= (r0[k] * q0[t, :] @ a0) - (r0[t] * q0[k, :] @ a0) - (r1[k] * q1[t, :] @ a1) + (r1[t] * q1[k, :] @ a1)

		p_n_den = ms + (r0[k] * q0[t, :] @ b0) - (r0[t] * q0[k, :] @ b0)
		p_n_den += -(r1[k] * q1[t, :] @ b1) + (r1[t] * q1[k] @ b1)

		p_n = (self.springiness + 1) * (p_n_nom / p_n_den)

		return np.array([
			p_n,
			a_t + b_t * p_n,
			a_k + b_k * p_n
		])

	def __max_p(self, u0, u1, w0, w1, ms, q0, q1, r0, r1):
		"""Impuls maksymalny przy braku zrownania predkosci (C.4.1)"""
		phi = np.array([  # Wzory 66, 67, 68
			u0[n] - u1[n] - (w0[k] * r0[t]) + (w0[t] * r0[k]) + (w1[k] * r1[t]) - (w1[t] * r1[k]),  # n
			u0[t] - u1[t] - (w0[k] * r0[n]) - (w0[n] * r0[k]) - (w1[k] * r1[n]) + (w1[n] * r1[k]),  # t
			u0[k] - u1[k] - (w0[t] * r0[n]) + (w0[n] * r0[t]) + (w1[t] * r1[n]) - (w1[n] * r1[t])  # k
		])

		# Wzor 63
		mi_t = np.abs(self.friction * phi[t] / np.sqrt(phi[t]**2 + phi[k]**2))
		mi_t *= np.sign(phi[t] / phi[n])

		# Wzor 65
		mi_k = np.abs(self.friction * phi[k] / np.sqrt(phi[t]**2 + phi[k]**2))
		mi_k *= np.sign(phi[k] / phi[n])

		c0 = np.array([  # Wzor 69
			-(r0[k] * mi_t) + (r0[t] * mi_k),
			r0[k] - (r0[n] * mi_k),
			-r0[t] + (r0[n] * mi_t)
		])

		c1 = np.array([  # Wzor 70
			(r1[k] * mi_t) - (r1[t] * mi_k),
			-r1[k] + (r1[n] * mi_k),
			r1[t] - (r1[n] * mi_t)
		])

		p_n_nom = (u1[n] - u0[n]) - (r0[k] * w0[t]) + (r0[t] * w0[k]) + (r1[k] * w1[t]) - (r1[t] * w1[k])
		p_n_den = ms + (r0[k] * q0[t, :] @ c0) - (r0[t] * q0[k, :] @ c0) - (r1[k] * q1[t, :] @ c1) - (r1[t] * q1[k, :] @ c1)

		p_n = (self.springiness + 1) * (p_n_nom / p_n_den)

		return np.array([
			p_n,
			p_n * mi_t,
			p_n * mi_k
		])

	def __vel_post_collision(self, other, m, x, ms, q0, q1):
		"""Wybiera impuls (max/kryt) i zwraca predkosci  obu obiektow z uzyciem tego impulsu"""
		# Predkosci liniowe cial przed kolizja
		u0 = m @ self.lin_velocity
		u1 = m @ other.lin_velocity

		# Wektor od srodka ciezkosci ciala do punktu kolizji
		r0 = m @ (x - self.mass_center)
		r1 = m @ (x - other.mass_center)

		# Predkosci katowe cial przed kolizja
		w0 = m @ self.ang_velocity
		w1 = m @ other.ang_velocity

		# Obliczenie impulsu krytycznego i maksymalnego
		crit_p = self.__crit_p(u0, u1, w0, w1, ms, q0, q1, r0, r1)
		max_p = self.__max_p(u0, u1, w0, w1, ms, q0, q1, r0, r1)

		use_crit = np.linalg.norm(max_p) > np.linalg.norm(crit_p)
		p = crit_p if use_crit else max_p

		# Wybor impulsow (C.4.3)
		v0 = (p / self.mass) + u0
		e0 = (q0 * r0) @ p + w0

		v1 = (p / other.mass) + u1
		e1 = (q1 * r1) @ p + w1

		return p, v0, e0, v1, e1

	def __sim_collision(self, other, tri_index, coll_vertex, dt, debug_info=False):
		v0, v1, v2 = other.model.triangle(tri_index)

		# Cofnij translacje o jeden krok
		self.model.translate(-self.lin_velocity * dt)

		# Wyznacz wektor normalny plaszczyzny (wzor 94)
		tri_normal = np.cross(v1 - v0, v2 - v0)
		tri_u = tri_normal @ v0

		# Rzutuj punkt na plaszczyzne (wzory 95, 96, 97)
		cross_const = tri_u - (tri_normal * (coll_vertex * [1, -1, -1])).sum()
		cross_const /= (tri_normal**2).sum()
		x = coll_vertex + tri_normal * cross_const

		# Przejscie z xyz do ukladu ntk
		m = utils.xyz2ntk(x, coll_vertex, norm=True)

		# Rysowanie wektorow ntk
		if debug_info:
			# print(f"Odbicie przy wektorze n: {m[0, :]}")
			glBegin(GL_LINES)
			for i in range(m.shape[0]):
				glColor(*np.eye(3)[i, :])  # rgb -> ntk
				glVertex3f(*self.mass_center)
				glVertex3f(*(self.mass_center + m[i, :] * 10))
			glEnd()

		ms = (self.mass + other.mass) / (self.mass * other.mass)

		g0 = m @ self.j_matrix @ m.transpose()  # Wzor 39
		q0 = np.linalg.inv(g0)

		g1 = m @ other.j_matrix @ m.transpose()
		q1 = np.linalg.inv(g1)

		p, v0, e0, v1, e1 = self.__vel_post_collision(other, m, x, ms, q0, q1)

		self.lin_velocity = np.linalg.inv(m) @ v0
		self.ang_velocity = np.linalg.inv(m) @ e0

		other.lin_velocity = np.linalg.inv(m) @ v1
		other.ang_velocity = np.linalg.inv(m) @ e1

	def collide(self, other, dt):
		"""Symuluje kolizje w przypadku jej wystapienia"""

		# Nie ma sensu sprawdzac dalej, jesli nie ma kolizji modeli
		if not coll.aabb_model_collision_3d(self.model, other.model):
			return

		# Sprawdz, ktory wierzcholek przecial sie z plaszczyzna
		for i in range(other.model.tris_len):
			tri = other.model.triangle(i)
			for vertex in self.model.vertices:
				dist = utils.point_plane_dist(vertex, tri)
				if dist < 0.2:
					self.__sim_collision(other, i, vertex, dt, debug_info=True)

	def render(self, **kwargs):
		self.model.render(**kwargs)

	def update(self, dt):
		self._prev_lin_vel = self.lin_velocity
		self.lin_velocity += dt * (self.delta_o - np.array([0, 9.81, 0])) * self.lin_resistance
		self.model.translate(self.lin_velocity * dt)

		self._prev_o = self.o
		forces_sum = np.sum(self.mass / 1000 * self.delta_lin_vel for _ in self.model.vertices) * dt
		self.o = np.sum(forces_sum * v.transpose() - self.mass_center for v in self.model.vertices)

		self.ang_velocity *= self.ang_resistance
		mc = self.mass_center
		for i in range(len(self.model.vertices)):
			vp = (self.model.vertices[i, :] - mc) @ utils.r3_matrix(*self.ang_velocity * dt) + mc
			self.model.vertices[i, :] = vp
