import numpy as np

from app import Application
from model import Model
from utils import MouseControllerMixin
from physics import RigidBody


class Zad1App(MouseControllerMixin, Application):
	environment = []
	tetra = None

	def __setup_env(self):
		d = 100000

		floor_model = Model.create_box(
			(-10, 0, -10), (10, 1, 10), colors=(0.4, 0.1, 0)
		)

		floor_model.rotate(np.deg2rad(25), axis=(0, 0, 1))
		floor_model.translate((0, -5, 0))

		self.environment.append(RigidBody(floor_model, density=d))

		ceil_model = Model.create_box(
			(-10, 0, -10), (10, 1, 10), colors=(0.2, 0, 0.3)
		)
		ceil_model.translate((0, 5, 0))
		ceil_model.rotate(np.deg2rad(20), axis=(1, 0, 0))

		self.environment.append(RigidBody(ceil_model, density=d))

	def __setup_tetra(self):
		tetra_model = Model.create_tetrahedron([0, 0, 0], [2, 0, 0], [0, 2, 0], [1, 1, 2])
		tetra_model.translate((0, -1, 0))

		self.tetra = RigidBody(
			tetra_model,
			density=0.7,
			springiness=0.4,
			friction=0.9,
			lin_resist=0.6,
			ang_resist=0.7
		)
		self.tetra.lin_velocity -= np.array([0.5, 6, -0.7])
		self.tetra.ang_velocity += np.array([0.03, -0.09, 0.06])

	def init_resources(self):
		self.set_global_light_pos(position=(0, 5, 5))
		self.__setup_env()
		self.__setup_tetra()

	def on_keyboard(self, key, x, y):
		if key == b" ":
			self.__setup_tetra()

	def on_render(self):
		self.look_at(
			cam_pos=(self.cam_dist * np.sin(self.t), 3, self.cam_dist * np.cos(self.t)),
			target_pos=(0, 0, 0)
		)

		self.tetra.render()
		for rb in self.environment:
			rb.render(wireframe=True)

		self.tetra.update(self.tick_delta)
		for rb in self.environment:
			self.tetra.collide(rb, self.tick_delta)


if __name__ == "__main__":
	Zad1App(
		resolution=(1024, 768),
		clear_color=(0, 0, 0, 1),
		title="Lista 10, zadanie 1"
	).run()
