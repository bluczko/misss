from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *
import numpy as np
import time

class Application(object):
	def __init__(
		self,
		resolution=(800, 600), clear_color=(1, 1, 1, 1), title="",
		target_fps=60, proj_type="persp", light_pos=None
	):
		self.resolution = resolution
		self.title = title
		self.clear_color = clear_color
		self.tick = 0
		self.target_fps = target_fps
		self.proj_type = proj_type

		self.enable_lighting = light_pos is not None

		if self.enable_lighting:
			self.global_light_pos = np.array(light_pos)

	@property
	def tick_delta(self):
		return 1.0 / self.target_fps

	def translate(self, offset):
		glTranslate(*offset)

	def look_at(self, cam_pos, target_pos, up_pos=(0, 1, 0)):
		gluLookAt(*cam_pos, *target_pos, *up_pos)

	def run(self):
		glutInit()
		glutInitWindowSize(*self.resolution)
		glutCreateWindow(self.title)

		glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH)

		glutDisplayFunc(self.__idle)
		glutIdleFunc(self.__idle)
		glutMotionFunc(lambda x, y: self.on_motion(x, y))
		glutMouseFunc(lambda button, state, x, y: self.on_mouse(button, not state, x, y))
		glutKeyboardFunc(lambda key, x, y: self.on_keyboard(key, x, y))

		glClearColor(*self.clear_color)
		glClearDepth(1)

		glDepthFunc(GL_LESS)
		glEnable(GL_DEPTH_TEST)
		glEnable(GL_TEXTURE_2D)

		if self.enable_lighting:
			glEnable(GL_LIGHT0)
			glLight(GL_LIGHT0, GL_POSITION, self.global_light_pos)
			glEnable(GL_LIGHTING)
			glEnable(GL_COLOR_MATERIAL)

		glMatrixMode(GL_PROJECTION)
		glLoadIdentity()

		near, far = 10**-4, 10**4

		if self.proj_type == 'persp':
			aspect_ratio = float(self.resolution[0]) / float(self.resolution[1])
			gluPerspective(50, aspect_ratio, near, far)

		elif self.proj_type == 'ortho':
			glOrtho(-10, 10, -10, 10, near, far)

		glMatrixMode(GL_MODELVIEW)
		self.init_resources()

		glutMainLoop()

	def enable_wireframe(self, wireframe):
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE if wireframe else GL_FILL)

	def set_global_light_pos(self, position):
		if self.enable_lighting:
			self.global_light_pos = np.array(position)
			glLight(GL_LIGHT0, GL_POSITION, self.global_light_pos)

	def __update_required(self):
		ltime = time.clock()

		if ltime < self.tick + self.tick_delta:
			return False

		self.tick = ltime
		return True

	def __idle(self):
		if self.__update_required():
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
			glLoadIdentity()
			self.on_render()
			glutSwapBuffers()

	def init_resources(self):
		pass

	def on_render(self):
		pass

	def on_motion(self, x, y):
		pass

	def on_mouse(self, button, state, x, y):
		pass

	def on_keyboard(self, key, x, y):
		pass
