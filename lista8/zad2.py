from app import Application
from model import Model
from collisions import model_collision_2d, model_inside_other_2d

class Zad2App(Application):
	quad0 = Model.create_quad(c0=(-2, -4), c1=(2, 4))
	quad1 = Model.create_quad(c0=(-6, -1), c1=(-4, 1))

	def on_keyboard(self, key, x, y):
		quad = self.quad0

		move_speed = self.tick_delta * 10
		rot_speed = self.tick_delta * 2

		if key == b"w":
			quad.translate((0, move_speed, 0))
		elif key == b"s":
			quad.translate((0, -move_speed, 0))
		elif key == b"a":
			quad.translate((-move_speed, 0, 0))
		elif key == b"d":
			quad.translate((move_speed, 0, 0))
		elif key == b"q":
			quad.rotate(-rot_speed)
		elif key == b"e":
			quad.rotate(rot_speed)

	def on_render(self):
		self.look_at(cam_pos=(0, 0, 15), target_pos=(0, 0, 0))
		self.quad1.rotate(self.tick_delta)

		exact_collision = model_collision_2d(self.quad0, self.quad1)
		inside01 = model_inside_other_2d(self.quad0, self.quad1)
		inside10 = model_inside_other_2d(self.quad1, self.quad0)
		color = (1, 1, 0, 1) if exact_collision else (0, 1, 0, 1)
		print(f"Exact collision: {exact_collision}, quad0 in quad1: {inside01}, quad1 in quad0: {inside10}")

		self.quad0.colors = color
		self.quad1.colors = (1, 0, 0, 1) if inside01 else color
		self.quad0.render(wireframe=True, mode="poly")
		self.quad1.render(wireframe=True)


if __name__ == "__main__":
	Zad2App(
		resolution=(1024, 768),
		clear_color=(0, 0, 0, 1),
		title="Lista 8, zadanie 2",
		proj_type="ortho"
	).run()
