from app import Application
from collisions import *
from OpenGL.GL import *
import numpy as np


class Robot:
	parts = []
	position = [0, 0, 0]
	rotation = 0

	def __init__(self):
		body = Model.create_box([-1, -0.5, -1.5], [1, 0.5, 1.5], colors=(1, 0, 0, 1))
		self.parts.append(body)

		head = Model.create_ngon(12, 0.5, 1.5, colors=(0, 0, 1, 1))
		head.rotate(-np.pi/2, (1, 0, 0), center=(0, 0, 0))
		head.translate((0, 0, 1.5))
		self.parts.append(head)

		right_wheel = Model.create_ngon(12, 1, 1, colors=(1, 1, 0, 1))
		right_wheel.rotate(np.pi/2, (0, 0, 1))
		right_wheel.translate((-1, 0, 0))
		self.parts.append(right_wheel)

		left_wheel = Model.create_ngon(12, 1, 1, colors=(0, 1, 1, 1))
		left_wheel.rotate(-np.pi/2, (0, 0, 1))
		left_wheel.translate((1, 0, 0))
		self.parts.append(left_wheel)

	def translate(self, pos):
		for part in self.parts:
			part.translate(pos)

	def rotate(self, rad):
		self.rotation += rad

		robot_center = self.parts[0].vertices.mean(axis=0)

		for part in self.parts:
			part.rotate(rad, center=robot_center, axis=(0, 1, 0))

	def render(self, show_aabb=False, wireframe=False):
		glRotate(self.rotation, 0, 1, 0)
		glTranslate(*self.position)

		for part in self.parts:
			part.render(wireframe=wireframe)

			if show_aabb:
				aabb_render(part)

	def collides(self, other):
		for part in self.parts:
			# sprawdzaj najpierw AABB, bo jest o wiele szybsze
			if aabb_model_collision_3d(part, other):
				if model_collision_3d(part, other):
					return True

		return False


class Zad5App(Application):
	environment = []
	robot = Robot()
	show_aabb = True
	rotation = True

	def __init__(self, **kwargs):
		super(Zad5App, self).__init__(**kwargs)

		for i in range(3):
			box = Model.create_cube(0.5)
			box.translate(np.random.randn(1, 3))
			self.environment.append(box)

	def on_keyboard(self, key, x, y):
		model = self.robot

		move_speed = self.tick_delta * 10
		rot_speed = self.tick_delta * 2

		dx = np.sin(model.rotation) * move_speed
		dz = np.cos(model.rotation) * move_speed

		if key == b"w":
			model.translate((-dx, 0, dz))
		elif key == b"s":
			model.translate((dx, 0, -dz))
		elif key == b"a":
			model.rotate(-rot_speed)
		elif key == b"d":
			model.rotate(rot_speed)
		elif key == b"c":
			self.show_aabb = not self.show_aabb

	def on_render(self):
		robot_center = self.robot.parts[0].vertices.mean(axis=0)
		self.look_at(cam_pos=(0, 20, 5), target_pos=robot_center)
		self.robot.render(show_aabb=self.show_aabb, wireframe=True)

		for box in self.environment:
			if self.robot.collides(box):
				box.colors = (1, 0, 0, 1)
			else:
				box.colors = (1, 1, 1, 1)

			box.render()


if __name__ == "__main__":
	Zad5App(
		resolution=(1024, 768),
		clear_color=(0, 0, 0, 1),
		title="Lista 8, zadanie 5"
	).run()
