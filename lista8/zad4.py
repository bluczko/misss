from app import Application
from model import Model
from collisions import aabb_render, model_collision_3d
from numpy import random, linalg

def vec_norm(vec):
	return vec / linalg.norm(vec)

class Zad4App(Application):
	box0 = Model.create_box(c0=(-2, -4, -2), c1=(2, 4, 2), colors=list(random.rand(8, 4)))
	box1 = Model.create_box(c0=(-6, -1, -1), c1=(-4, 1, 1), colors=list(random.rand(8, 4)))

	tetra0 = Model.create_tetrahedron(
		[0, 0, 0], [2, 0, 0], [0, 2, 0], [1, 1, 2],
		colors=[(1, 0, 0, 1), (0, 1, 0, 1), (1, 1, 0, 1), (0, 1, 1, 1)]
	)

	tetra1 = Model.create_tetrahedron(
		[3, 0, 0], [5, 0, 0], [3, 2, 0], [4, 1, 2],
		colors=[(1, 0, 0, 1), (0, 1, 0, 1), (1, 1, 0, 1), (0, 1, 1, 1)]
	)

	ico = Model.create_icosahedron(
		colors=list(random.rand(12, 4))
	)

	render_list = [box0, box1, tetra0, tetra1, ico]

	show_aabb = True
	rotation = True

	def on_keyboard(self, key, x, y):
		model = self.box0

		move_speed = self.tick_delta * 10
		rot_speed = self.tick_delta * 2

		if key == b"w":
			model.translate((0, move_speed, 0))
		elif key == b"s":
			model.translate((0, -move_speed, 0))
		elif key == b"a":
			model.translate((-move_speed, 0, 0))
		elif key == b"d":
			model.translate((move_speed, 0, 0))
		elif key == b"q":
			model.rotate(-rot_speed)
		elif key == b"e":
			model.rotate(rot_speed)
		elif key == b"r":
			model.rotate(rot_speed, axis=(1, 0, 0))
		elif key == b"f":
			model.rotate(-rot_speed, axis=(1, 0, 0))
		elif key == b"c":
			self.show_aabb = not self.show_aabb
		elif key == b"x":
			self.rotation = not self.rotation

	def on_render(self):
		if self.rotation:
			self.box1.rotate(self.tick_delta, axis=(0, 0, 1), center=(0, 0, 0))

		self.look_at(cam_pos=(0, 20, 1), target_pos=(0, 0, 0))

		exact_coll = model_collision_3d(self.box0, self.box1)
		color = (1, 0, 0, 1) if exact_coll else (0, 1, 0, 1)

		print(f"Exact: {exact_coll}")

		for model in self.render_list:
			model.render(mode="tri")

			if self.show_aabb:
				aabb_render(model, color)


if __name__ == "__main__":
	Zad4App(
		resolution=(1024, 768),
		clear_color=(0, 0, 0, 1),
		title="Lista 8, zadanie 4"
	).run()
