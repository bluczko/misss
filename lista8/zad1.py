from app import Application
from model import Model
from collisions import aabb_model_collision_2d

class Zad1App(Application):
	quad0 = Model.create_quad(c0=(-2, -4), c1=(2, 4))
	quad1 = Model.create_quad(c0=(-6, -1), c1=(-4, 1))
	show_aabb = True

	def on_keyboard(self, key, x, y):
		quad = self.quad0

		move_speed = self.tick_delta * 10
		rot_speed = self.tick_delta * 2

		if key == b"w":
			quad.translate((0, move_speed, 0))
		elif key == b"s":
			quad.translate((0, -move_speed, 0))
		elif key == b"a":
			quad.translate((-move_speed, 0, 0))
		elif key == b"d":
			quad.translate((move_speed, 0, 0))
		elif key == b"q":
			quad.rotate(-rot_speed)
		elif key == b"e":
			quad.rotate(rot_speed)
		elif key == b"c":
			self.show_aabb = not self.show_aabb

	def on_render(self):
		self.look_at(cam_pos=(0, 0, 15), target_pos=(0, 0, 0))
		self.quad1.rotate(self.tick_delta, center=(0, 0, 0))

		aabb_collision = aabb_model_collision_2d(self.quad0, self.quad1, render_box=self.show_aabb)
		print(f"AABB collision: {aabb_collision}")

		self.quad0.render()
		self.quad1.render()


if __name__ == "__main__":
	Zad1App(
		resolution=(1024, 768),
		clear_color=(0, 0, 0, 1),
		title="Lista 8, zadanie 1",
		proj_type="ortho"
	).run()
